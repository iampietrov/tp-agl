
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import fr.umfds.agl.terproject.GestionTER;
import fr.umfds.agl.terproject.Groupe;
import fr.umfds.agl.terproject.Hongrois;
import fr.umfds.agl.terproject.Sujet;

@ExtendWith(MockitoExtension.class)
class TestAffectation {
	
	@Mock
	Hongrois h;
	
	@BeforeEach
	void init() {
		GestionTER.getInstance().setHongrois(h);
	}
	
	@Test
	void testAffectationSujetGroupe() {
		// given groupe g et sujet s
		Groupe g = new Groupe("1");
		Sujet s = new Sujet(null, "1");
		
		// when 
		g.setSujetAffecté(s);
		
		// then
		assertEquals(g.getSujetAffecté(), s);
		assertEquals(s.getGroupeAffecté(), g);
	}
	
	@Test
	void testAffectationHongrois() {
		
		// given		
		GestionTER.getInstance().init();
		
		when(h.affectation(1)).thenReturn(List.of(List.of(1,1), List.of(2,3)));
		
		// when
		GestionTER.getInstance().affectation(1);
		
		// then
		assert(true);		
	}
	
}

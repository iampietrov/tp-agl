package fr.umfds.agl.terproject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class GestionTER {
	
	private static GestionTER instance;
	
	private HashMap<String,Groupe> groupes = new HashMap<String, Groupe>();
	private ObjectMapper mapper = new ObjectMapper();
	private HashMap<String, Sujet> sujets = new HashMap<String, Sujet>();
	private Hongrois gestionnaireAffectation;
	
	private GestionTER() {
		if (instance==null) {	
			initMapper();
			instance=this;
		}
		
	}
	
	public static GestionTER getInstance() {
		if (instance!=null) {
			return instance;
		} else {
			return new GestionTER();
		}
	}

	public Sujet getSujet(String idSujet) {
		return sujets.get(idSujet);
	}
	
	public void setHongrois(Hongrois h) {
		gestionnaireAffectation = h;
	}
	
	public void affectation(int passe) {
		
		gestionnaireAffectation.setHauteur(groupes.size());
		gestionnaireAffectation.setLargeur(sujets.size());
		
		List<List<Integer>> adjList = new ArrayList<List<Integer>>();
		List<Integer> choices = new ArrayList<Integer>();
		
//		for (Groupe g : groupes.values()) {
//			for (Sujet s : sujets.values()) {
//				if (g.getChoixSujets().containsKey(s.getIdSujet())) 
//					choices.add(g.getChoixSujets().get(s.getIdSujet()));
//				else choices.add(0);
//			}
//		}
		
		gestionnaireAffectation.setAdjacenceList(adjList);
		for (List<Integer> coupleGrSujet : gestionnaireAffectation.affectation(passe)) {
			Groupe g = groupes.get(coupleGrSujet.get(0).toString());
			Sujet s = sujets.get(coupleGrSujet.get(1).toString());
			
			if (g != null && s != null) g.setSujetAffecté(s);
		}
		
	}
	
	private void initMapper() {
		SimpleModule module = 
		    	  new SimpleModule("GroupeSerializer", new Version(1, 0, 0, null, null, null));
		    	module.addSerializer(Groupe.class, new GroupeSerializer());
		    	module.addDeserializer(Groupe.class, new GroupeDeserializer());
		    	mapper.registerModule(module);
	}
	
	public void init() {
		
		groupes.put("1", new Groupe("Groupe 1"));
		groupes.put("2", new Groupe("Groupe 2"));
		groupes.put("3", new Groupe("Groupe 3"));
		
		sujets.put("1", new Sujet());
		sujets.put("2", new Sujet());
		sujets.put("3", new Sujet());
	
	}
	
	public void serializeGroupes() throws JsonGenerationException, JsonMappingException, IOException {
		// avec serialiseur ad hoc
    	
    	mapper.writeValue(new File("src/main/resources/groupes.json"), groupes.values());
    	
	}
	public void importGroupes() throws JsonGenerationException, JsonMappingException, IOException {
		List<Groupe> gliste = mapper.readValue(new File("src/main/resources/groupes.json"), 
												new TypeReference<List<Groupe>>(){});
    	for (Groupe g:gliste) {
    		groupes.put(g.getIdGroupe(), g);
    	}
	}

	@Override
	public String toString() {
		return "GestionTER [groupes=" + groupes + ", sujets=" + sujets + "]";
	}
	
}
